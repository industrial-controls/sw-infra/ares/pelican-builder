FROM python:3.7-slim

RUN apt update && apt install -y git rsync build-essential && \
    rm -rf /var/lib/apt/lists/*

ENV PELICAN_PLUGINS  /opt/pelican/plugins
ENV PELICAN_THEMES  /opt/pelican/themes

RUN mkdir -p $PELICAN_PLUGINS && \
    mkdir -p $PELICAN_THEMES

# The "git clone" is not recursive, as the theme we want is hosted in the repository
RUN cd /tmp && git clone https://gitlab.cern.ch/industrial-controls/sw-infra/ares/pelican-themes.git && \
    cp -r /tmp/pelican-themes/pelican-bootstrap3 $PELICAN_THEMES && \
    rm -rf /tmp/pelican-themes

RUN cd /tmp && git clone --recursive https://gitlab.cern.ch/industrial-controls/sw-infra/ares/pelican-plugins.git && \
    cp -r /tmp/pelican-plugins/better_tables $PELICAN_PLUGINS && \
    cp -r /tmp/pelican-plugins/i18n_subsites $PELICAN_PLUGINS && \
    cp -r /tmp/pelican-plugins/pelican-datatable $PELICAN_PLUGINS && \
    cp -r /tmp/pelican-plugins/tipue_search $PELICAN_PLUGINS && \
    rm -rf /tmp/pelican-plugins

RUN pip install pelican[markdown] pelican-search beautifulsoup4 future pyyaml $PELICAN_PLUGINS/tipue_search

RUN pelican-themes -i $PELICAN_THEMES/pelican-bootstrap3
