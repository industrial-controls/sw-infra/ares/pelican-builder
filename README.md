# Pelican Builder Image

A container image to build ARES website using Pelican

## Why another builder image ?

Pelican is unfortunately not (yet) a very pythonic website generator:
* Most plugins are not yet available as ```pip install```able packages.
* Themes are not available as ```pip install```able packages at all.
* Custom pelican tools must be used to install themes into the python environment.

This container places all the elements we need in well-known locations to ease website generation.

## How to use the image

Please refer to the Dockerfile for contents.

Based on a slim Debian Python image, important paths, available as environment variables :

```bash
PELICAN_PLUGINS=/opt/pelican/plugins
PELICAN_THEMES=/opt/pelican/themes
```
Upon starting the image, the ``pelican-bootstrap3`` theme is pre-installed.
Your settings must refer to the ```$PELICAN_PLUGINS``` environment variable to locate the required plugins.